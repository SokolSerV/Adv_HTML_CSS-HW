"use strict";

function menuVisible() {
  document
    .querySelector(".page-header__bars")
    .addEventListener("click", function () {
      document
        .querySelector(".page-header__menu")
        .classList.toggle("menu-visible");

      let bars = document.querySelector(".bars-icon");
      let xmark = document.querySelector(".xmark-icon");

      if (bars.style.display !== "none") {
        bars.style.display = "none";
        xmark.style.display = "";
      } else {
        bars.style.display = "";
        xmark.style.display = "none";
      }
    });
}
menuVisible();
